<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->
# Kt Isar

A litte extension to convert to immutable objects from [kt_dart](https://pub.dev/packages/kt_dart)

## Features

KtQuery
```dart    
Future<KtList> findAllKt()
KtList findAllKtSync()
Future<KtList<KtMap<String, dynamic>>> exportJsonKt()
KtList<KtMap<String, dynamic>> exportJsonKtSync()
```

KtIsarCollection
```dart  
Future<KtList> getAllKt(KtList<int>) 
KtList getAllKtSync(KtList<int>) 
Future<KtList<int>> putAllKt(KtList,...) 
KtList<int> putAllKtSync(KtList,...) 
Future<int> deleteAllKt(KtList<int>) 
int deleteAllKtSync(KtList<int>) 
Future<void> importJsonKt(KtList<KtMap>,...) 
void importJsonSyncKt(KtList<KtMap>,...) 
```

KtQueryExecute
```dart
Future<KtList> findAllKt()
KtList findAllKtSync()
Stream<KtList> watchKt({...}) 
Future<KtList<KtMap<String, dynamic>>> exportJsonKt()
```

## Getting started

1. Install [isar](https://pub.dev/packages/isar)  
1. Install kt_isar
```
flutter pub get kt_isar
```

## Usage
Import either `kt_isar.dart` or `kt_isar_ext.dart` only for extensions.
