```dart
import 'package:flutter/cupertino.dart';
import 'package:kt_dart/kt.dart';
import 'package:kt_isar/kt_isar.dart';
import 'package:path_provider/path_provider.dart';

part 'main.g.dart';

Future<void> main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  await StorageService().init();
  final bob = Person('Bobby');
  final maja = Person('Maja');
  final albert = Person('Albert');

  final persons = KtList.of(bob, maja, albert);

  final ids = StorageService().addPersons(persons);

  print(StorageService().getPersons(ids).toString());
}

class StorageService {
  Isar? _isar;
  static final StorageService _instance = StorageService._internal();
  factory StorageService() => _instance;

  StorageService._internal();

  Future<void> init() async {
    _isar = await _startIsar();
  }

  Future<Isar> _startIsar() async {
    final dir = await getApplicationSupportDirectory();
    return await Isar.open(
      schemas: [PersonSchema],
      directory: dir.path,
    );
  }

  KtList<int> addPersons(KtList<Person> persons) {
    return _isar!.writeTxnSync((isar) => isar.persons.putAllKtSync(persons));
  }

  KtList<Person?> getPersons(KtList<int> ids) {
    return _isar!.writeTxnSync<KtList<Person?>>(
        (isar) => isar.persons.getAllKtSync(ids));
  }
}

@Collection()
class Person {
  int? id;
  final String name;

  Person(this.name);

  @override
  String toString() {
    return 'Person(name:$name)';
  }
}

```