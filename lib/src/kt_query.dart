import 'package:isar/isar.dart';
import 'package:kt_dart/kt.dart';

/// Extension to return [KtList] instead of [List] for [Query] objects
extension KtQuery<T> on Query<T> {
  /// {@macro query_find_all}
  Future<KtList<T>> findAllKt() => findAll().then(
        (list) => list.toImmutableList(),
      );

  /// {@macro query_find_all}
  KtList<T> findAllKtSync() => findAllSync().toImmutableList();

  /// {@macro query_watch}
  Stream<KtList<T>> watchKt({bool initialReturn = false}) => watch().map(
        (list) => list.toImmutableList(),
      );

  /// {@macro query_export_json}
  Future<KtList<KtMap<String, dynamic>>> exportJsonKt() => exportJson().then(
        (list) => list.map((item) => item.toImmutableMap()).toImmutableList(),
      );

  /// {@macro query_export_json}
  KtList<KtMap<String, dynamic>> exportJsonKtSync() =>
      exportJsonSync().map((item) => item.toImmutableMap()).toImmutableList();
}
