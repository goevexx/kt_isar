import 'package:isar/isar.dart';
import 'package:kt_dart/kt.dart';

extension KtQueryExecute<OBJ, R> on QueryBuilder<OBJ, R, QQueryOperations> {
  /// {@macro query_find_all}
  Future<KtList<R>> findAllKt() => build().findAll().then(
        (list) => list.toImmutableList(),
      );

  /// {@macro query_find_all}
  KtList<R> findAllKtSync() => build().findAllSync().toImmutableList();

  /// {@macro query_watch}
  Stream<KtList<R>> watchKt({bool initialReturn = false}) => build()
      .watch(initialReturn: initialReturn)
      .map((list) => list.toImmutableList());

  /// {@macro query_export_json}
  Future<KtList<KtMap<String, dynamic>>> exportJsonKt() =>
      build().exportJson().then(
            (list) => list.map((map) => map.toImmutableMap()).toImmutableList(),
          );
}
