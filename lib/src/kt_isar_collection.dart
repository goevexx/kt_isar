import 'package:isar/isar.dart';
import 'package:kt_dart/kt.dart';

/// Extension to return [KtList] instead of [List] for [IsarCollection] objects
extension KtIsarCollection<OBJ> on IsarCollection<OBJ> {
  /// Get a list of objects by their [ids] or `null` if an object does not
  /// exist.
  Future<KtList<OBJ?>> getAllKt(KtList<int> ids) =>
      getAll(ids.asList()).then((list) => list.toImmutableList());

  /// Get a list of objects by their [ids] or `null` if an object does not
  /// exist.
  KtList<OBJ?> getAllKtSync(KtList<int> ids) =>
      getAllSync(ids.asList()).toImmutableList();

  /// Insert or update a list of [objects] and returns the list of assigned ids.
  Future<KtList<int>> putAllKt(
    KtList<OBJ> objects, {
    bool replaceOnConflict = false,
    bool saveLinks = false,
  }) =>
      putAll(objects.asList()).then((list) => list.toImmutableList());

  /// Insert or update a list of [objects] and returns the list of assigned ids.
  KtList<int> putAllKtSync(
    KtList<OBJ> objects, {
    bool replaceOnConflict = false,
    bool saveLinks = false,
  }) =>
      putAllSync(objects.asList()).toImmutableList();

  /// Delete a list of objecs by their [ids].
  ///
  /// Returns the number of objects that have been deleted. Isar web always
  /// returns `ids.length`.
  Future<int> deleteAllKt(KtList<int> ids) => deleteAll(ids.asList());

  /// Delete a list of objecs by their [ids].
  ///
  /// Returns the number of objects that have been deleted.
  int deleteAllKtSync(KtList<int> ids) => deleteAllSync(ids.asList());

  /// Import a list of json objects.
  Future<void> importJsonKt(
    KtList<KtMap<String, OBJ?>> json, {
    bool replaceOnConflict = false,
  }) =>
      importJson(
        json.map((map) => map.asMap()).asList(),
      );

  /// Import a list of json objects.
  void importJsonSyncKt(
    KtList<KtMap<String, OBJ?>> json, {
    bool replaceOnConflict = false,
  }) =>
      importJsonSync(json.map((map) => map.asMap()).asList());
}
