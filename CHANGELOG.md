## 0.0.1

* Publish: combine kt_dart and isar basically

## 0.0.2

* Fix readme

## 0.0.2+0

* Fix readme
* Added usage in readme

## 0.0.3

* Added KtQueryBuilderExtensions
* Added KtQuery.exportJsonKt
* Added KtQuery.exportJsonKtSync
* Added tests

## 0.0.3+1

* Fixed readme

## 0.0.4

* Support dart only
## 0.0.5

* Added example
* Fixed generic extension issue
