import 'dart:typed_data';

import 'package:faker/faker.dart';
import 'package:isar/isar.dart';
import 'package:kt_dart/collection.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'kt_isar_test.mocks.dart';

int exportJsonRawSyncShim<R>(R Function(Uint8List)? callback) => 0;

@GenerateMocks(
  [],
  customMocks: [
    MockSpec<Query<int>>(
      as: #MockQueryOfInt,
      fallbackGenerators: {#exportJsonRawSync: exportJsonRawSyncShim},
    ),
    MockSpec<IsarCollection<int>>(
      as: #MockIsarCollectionOfInt,
    ),
    MockSpec<QueryBuilder<int, int, QWhere>>(
      as: #MockQueryBuilderOfInt,
    ),
  ],
)
void main() {
  late final List<int> dataList;
  late final KtList<int> dataKtList;
  late final Faker faker;
  late final List<Map<String, dynamic>> json;
  late final KtList<KtMap<String, dynamic>> jsonKt;

  setUpAll(() {
    faker = Faker();
    final randomNumber = Faker().randomGenerator.integer(100);
    dataList = faker.randomGenerator.numbers(randomNumber, randomNumber);
    dataKtList = KtList.from(dataList);

    final jsonCount = faker.randomGenerator.integer(randomNumber);
    json = [];
    for (int i = 0; i < jsonCount; i++) {
      json.add({
        i.toString(): i,
      });
    }
    jsonKt = json.map((map) => map.toImmutableMap()).toImmutableList();
  });

  group('KtQuery', () {
    late final Query<int> query;

    setUpAll(() {
      query = MockQueryOfInt();
    });

    test('Given a query When find all Then return an immutable list future',
        () async {
      // arrange
      when(query.findAll()).thenAnswer((_) async => dataList);
      // reflects KtQuery.findAllKt
      Future<KtList<int>> findAllKt() =>
          query.findAll().then((list) => list.toImmutableList());

      // act
      final immutableAll = findAllKt();

      // expect
      verify(query.findAll());
      expect(immutableAll, isA<Future<KtList<int>>>());
      expect(immutableAll, completion(equals(KtList.from(dataList))));
    });

    test(
        'Given a query When find all synchronously Then return an immutable list',
        () {
      // arrange
      when(query.findAllSync()).thenReturn(dataList);
      // reflects KtQuery.findAllKtSync
      KtList<int> findAllSyncKt() => query.findAllSync().toImmutableList();

      // act
      final immutableAll = findAllSyncKt();

      // expect
      verify(query.findAllSync());
      expect(immutableAll, isA<KtList<int>>());
      expect(immutableAll, equals(KtList.from(dataList)));
    });

    test('Given query When watching Then return an immutable list stream', () {
      // arrange
      when(query.watch())
          .thenAnswer((realInvocation) => Stream.fromIterable([dataList]));
      // reflects KtQuery.watchKt
      Stream<KtList<int>> watchKt() =>
          query.watch().map((list) => list.toImmutableList());

      // act
      final immutableWatchStream = watchKt();

      // expect
      // ignore: invalid_use_of_protected_member
      verify(query.watch());
      expect(immutableWatchStream, isA<Stream<KtList<int>>>());
      immutableWatchStream
          .listen(expectAsync1((list) => expect(list, dataKtList)));
    });

    test(
        'Given a query When export to json Then return a immutable list of immutable maps',
        () {
      // arrange
      when(query.exportJsonSync()).thenReturn(json);
      // reflects KtQuery.exportJsonKtSync
      KtList<KtMap<String, dynamic>> exportJsonKtSync() => query
          .exportJsonSync()
          .map((item) => item.toImmutableMap())
          .toImmutableList();

      // act
      final exportedJsonKt = exportJsonKtSync();

      // expect
      verify(query.exportJsonSync());
      expect(exportedJsonKt, isA<KtList<KtMap<String, dynamic>>>());
      expect(exportedJsonKt, equals(jsonKt));
    });

    test(
        'Given a query When export to json Then return a immutable list of immutable maps future',
        () {
      // arrange
      when(query.exportJson()).thenAnswer((_) async => json);
      // reflects KtQuery.exportJsonKt
      Future<KtList<KtMap<String, dynamic>>> exportJsonKt() =>
          query.exportJson().then(
                (list) =>
                    list.map((map) => map.toImmutableMap()).toImmutableList(),
              );

      // act
      final exportedJsonKtFuture = exportJsonKt();

      // expect
      expect(
        exportedJsonKtFuture,
        isA<Future<KtList<KtMap<String, dynamic>>>>(),
      );
      expect(exportedJsonKtFuture, completion(equals(jsonKt)));
    });
  });

  group('KtIsarCollection', () {
    late final IsarCollection<int> collection;

    setUpAll(
      () {
        collection = MockIsarCollectionOfInt();
      },
    );

    test(
        'Given a collection When get all Then get an immutable collection future',
        () {
      // arrange
      when(collection.getAll(dataList)).thenAnswer((_) async => dataList);
      // reflects KtIsarCollection.getAllKt
      Future<KtList<int?>> getAllKt(KtList<int> ids) => collection
          .getAll(ids.asList())
          .then((list) => list.toImmutableList());

      // act
      final immutableAll = getAllKt(dataKtList);

      // expect
      verify(collection.getAll(dataList));
      expect(immutableAll, isA<Future<KtList<int?>>>());
      expect(immutableAll, completion(equals(KtList.from(dataList))));
    });

    test(
        'Given a collection When get all synchronously Then get an immutable collection',
        () {
      // arrange
      when(collection.getAllSync(dataList)).thenReturn(dataList);
      KtList<dynamic> getAllKtSync(KtList<int> ids) =>
          collection.getAllSync(ids.asList()).toImmutableList();

      // act
      // reflects KtIsarCollection.getAllKt
      final immutableAll = getAllKtSync(dataKtList);

      // expect
      verify(collection.getAllSync(dataList));
      expect(immutableAll, isA<KtList<int?>>());
      expect(immutableAll, equals(KtList.from(dataList)));
    });

    test(
        'Given a collection When put all Then return immutable list of ids future',
        () {
      // arrange
      when(collection.putAll(dataList)).thenAnswer((_) async => dataList);
      // reflects KtIsarCollection.putAllKt
      Future<KtList<int?>> putAllKt(KtList<int> ids) => collection
          .putAll(ids.asList())
          .then((list) => list.toImmutableList());

      // act
      final immutableIds = putAllKt(dataKtList);

      // expect
      verify(collection.putAll(dataList));
      expect(immutableIds, isA<Future<KtList<int?>>>());
      expect(immutableIds, completion(equals(KtList.from(dataList))));
    });

    test(
        'Given a collection When put all synchronously Then return immutable list of ids',
        () {
      // arrange
      when(collection.putAllSync(dataList)).thenReturn(dataList);
      // reflects KtIsarCollection.putAllKtSync
      KtList<int> putAllKtSync(KtList<int> ids) =>
          collection.putAllSync(ids.asList()).toImmutableList();

      // act
      final immutableIds = putAllKtSync(dataKtList);

      // expect
      verify(collection.putAllSync(dataList));
      expect(immutableIds, isA<KtList<int>>());
      expect(immutableIds, equals(KtList.from(dataList)));
    });

    test(
        'Given a collection When delete all Then return number of deleted data future',
        () {
      // arrange
      const toBeDeleted = 0;
      when(collection.deleteAll(dataList)).thenAnswer((_) async => toBeDeleted);
      // reflects KtIsarCollection.deleteAllKt
      Future<int> deleteAllKt(KtList<int> ids) =>
          collection.deleteAll(ids.asList());

      // act
      final deletedData = deleteAllKt(dataKtList);

      // expect
      verify(collection.deleteAll(dataList));
      expect(deletedData, isA<Future<int>>());
      expect(deletedData, completion(equals(toBeDeleted)));
    });

    test(
        'Given a collection When delete all synchronously Then return number of deleted data',
        () {
      // arrange
      const toBeDeleted = 0;
      when(collection.deleteAllSync(dataList)).thenReturn(toBeDeleted);
      // reflects KtIsarCollection.deleteAllKtSync
      int deleteAllKtSync(KtList<int> ids) =>
          collection.deleteAllSync(ids.asList());

      // act
      final deletedData = deleteAllKtSync(dataKtList);

      // expect
      verify(collection.deleteAllSync(dataList));
      expect(deletedData, isA<int>());
      expect(deletedData, toBeDeleted);
    });

    test(
        'Given a collection When import json with immutable object Then succeed in future',
        () {
      // arrange
      when(collection.importJson(json));
      // reflects KtIsarCollection.importJsonKt
      Future<void> importJsonKt(KtList<KtMap<String, dynamic>> json) async {
        collection.importJson(json.map((map) => map.asMap()).asList());
      }

      // act
      final futureVoid = importJsonKt(jsonKt);

      // expect
      verify(collection.importJson(json));
      expect(futureVoid, isA<Future<void>>());
    });

    test(
        'Given a collection When import json with immutable object synchronously Then succeed',
        () {
      // arrange
      when(collection.importJsonSync(json));
      // reflects KtIsarCollection.importJsonKtSync
      Future<void> importJsonKtSync(KtList<KtMap<String, dynamic>> json) async {
        collection.importJsonSync(json.map((map) => map.asMap()).asList());
      }

      // act
      final futureVoid = importJsonKtSync(jsonKt);

      // expect
      verify(collection.importJsonSync(json));
      expect(futureVoid, isA<void>());
    });
  });

  group(
    'KtQueryExecute',
    () {
      late final QueryBuilder<int, int, QWhere> queryBuilder;
      late final Query<int> query;
      Query<int> build(QueryBuilder<int, int, QWhere> queryBuilder) =>
          // ignore: invalid_use_of_protected_member
          queryBuilder.buildInternal();

      setUpAll(
        () {
          query = MockQueryOfInt();
          queryBuilder = MockQueryBuilderOfInt();
        },
      );

      test('Given a querybuilder When find all Then immutable list future', () {
        // arrange
        // ignore: invalid_use_of_protected_member
        when(queryBuilder.buildInternal()).thenReturn(query);
        when(query.findAll()).thenAnswer(
          (realInvocation) async => dataList,
        );
        // reflects KtQueryExecute.findAllKt
        Future<KtList<int>> findAllKt() => build(queryBuilder).findAll().then(
              (list) => list.toImmutableList(),
            );

        // act
        final immutableAll = findAllKt();

        // expect
        // ignore: invalid_use_of_protected_member
        verify(queryBuilder.buildInternal());
        verify(query.findAll());
        expect(immutableAll, isA<Future<KtList<int>>>());
        expect(immutableAll, completion(equals(dataKtList)));
      });

      test(
          'Given a querybuilder When find all synchronously Then immutable list',
          () {
        // arrange
        // ignore: invalid_use_of_protected_member
        when(queryBuilder.buildInternal()).thenReturn(query);
        when(query.findAllSync()).thenReturn(dataList);
        // reflects KtQueryExecute.findAllKtSync
        KtList<int> findAllKtSync() =>
            build(queryBuilder).findAllSync().toImmutableList();

        // act
        final immutableAll = findAllKtSync();

        // expect
        // ignore: invalid_use_of_protected_member
        verify(queryBuilder.buildInternal());
        verify(query.findAllSync());
        expect(immutableAll, isA<KtList<int>>());
        expect(immutableAll, equals(dataKtList));
      });

      test(
          'Given querybuilder When watching Then return an immutable list stream',
          () {
        // arrange
        // ignore: invalid_use_of_protected_member
        when(queryBuilder.buildInternal()).thenReturn(query);
        when(query.watch())
            .thenAnswer((realInvocation) => Stream.fromIterable([dataList]));
        // reflects KtQueryExecute.watchKt
        Stream<KtList<int>> watchKt() =>
            build(queryBuilder).watch().map((list) => list.toImmutableList());

        // act
        final immutableAllStream = watchKt();

        // expect
        // ignore: invalid_use_of_protected_member
        verify(queryBuilder.buildInternal());
        verify(query.watch());
        expect(immutableAllStream, isA<Stream<KtList<int>>>());
        immutableAllStream
            .listen(expectAsync1((list) => expect(list, dataKtList)));
      });

      test(
          'Given a querybuilder When export to json Then return a immutable list of immutable maps future',
          () {
        // arrange
        // ignore: invalid_use_of_protected_member
        when(queryBuilder.buildInternal()).thenReturn(query);
        when(query.exportJson()).thenAnswer((_) async => json);
        // reflects KtQueryExecute.exportJsonKt
        Future<KtList<KtMap<String, dynamic>>> exportJsonKt() =>
            build(queryBuilder).exportJson().then(
                  (list) =>
                      list.map((map) => map.toImmutableMap()).toImmutableList(),
                );

        // act
        final exportedJsonKt = exportJsonKt();

        // expect
        // ignore: invalid_use_of_protected_member
        verify(queryBuilder.buildInternal());
        verify(query.exportJson());
        expect(exportedJsonKt, isA<Future<KtList<KtMap<String, dynamic>>>>());
        expect(exportedJsonKt, completion(equals(jsonKt)));
      });
    },
  );
}
